"""
MSBResponse_gen.py

Author: Kyle Crandall
Date: JANUARY 2021

Generates Mass-Spring-Damper Data and saves it
"""

import numpy as np
import matplotlib.pyplot as plt
import h5py

# Parameters:
K = 5
OMEGA = 10
TAU = 0.5
DT = 0.01
T_F = 2.5
VAR1 = 0.2
VAR2 = 0.5

def main():
    t = np.arange(0.0, T_F+DT, DT)
    x = K * (1 - np.exp(-t/TAU)*np.cos(OMEGA*t))
    n1 = np.random.normal(size=(t.shape[0],))*VAR1
    n2 = np.random.normal(size=(t.shape[0],))*VAR2

    plt.figure(1)
    plt.plot(t, x, '.')
    
    plt.figure(2)
    plt.plot(t, x+n1, '.')

    plt.figure(3)
    plt.plot(t, x+n2, '.')

    plt.show()

    with h5py.File("MSDResponse.h5", "w") as f:
        f.attrs["K"] = K
        f.attrs["tau"] = TAU
        f.attrs["omega_d"] = OMEGA
        clean_grp = f.create_group("clean")
        clean_grp.create_dataset("t", data=t)
        clean_grp.create_dataset("x", data=x)
        noise1_grp = f.create_group("noise1")
        noise1_grp.attrs["var"] = VAR1
        noise1_grp.create_dataset("t", data=t)
        noise1_grp.create_dataset("x", data=x+n1)
        noise2_grp = f.create_group("noise2")
        noise2_grp.attrs["var"] = VAR2
        noise2_grp.create_dataset("t", data=t)
        noise2_grp.create_dataset("x", data=x+n2)

if __name__ == "__main__":
    main()
