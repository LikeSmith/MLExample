"""
CIFAR_load.py

Author: Kyle Crandall
Date: JANUARY 2021

Load CIFAR10 dataset and save to hdf5
"""

import tensorflow as tf
import numpy as np
import h5py

VAL_SPLIT = 0.2
TST_SPLIT = 0.1


def main():
    (trn_img, trn_lab), (tst_img, tst_lab) = tf.keras.datasets.cifar10.load_data()
    imgs = np.concatenate([trn_img, tst_img], axis=0).astype("float32") / 255.0
    labs = np.concatenate([trn_lab, tst_lab], axis=0).astype("uint8")

    n = imgs.shape[0]

    labs_1h = np.zeros((n, 10))
    labs_1h[np.arange(n), labs] = 1.0

    n_tst = int(TST_SPLIT*n)
    n_val = int(VAL_SPLIT*n)
    n_trn = n - n_tst - n_val

    with h5py.File("cifar10.h5", "w") as f:
        trn_grp = f.create_group("train")
        trn_grp.create_dataset("img", data=imgs[:n_trn, ...])
        trn_grp.create_dataset("lab", data=labs[:n_trn])
        trn_grp.create_dataset("lab1h", data=labs_1h[:n_trn, ...])

        val_grp = f.create_group("valid")
        val_grp.create_dataset("img", data=imgs[n_trn:n_trn+n_val, ...])
        val_grp.create_dataset("lab", data=labs[n_trn:n_trn+n_val])
        val_grp.create_dataset("lab1h", data=labs_1h[n_trn:n_trn+n_val, ...])

        tst_grp = f.create_group("test")
        tst_grp.create_dataset("img", data=imgs[n_trn+n_val:, ...])
        tst_grp.create_dataset("lab", data=labs[n_trn+n_val:])
        tst_grp.create_dataset("lab1h", data=labs_1h[n_trn+n_val:, ...])

if __name__ == "__main__":
    main()
