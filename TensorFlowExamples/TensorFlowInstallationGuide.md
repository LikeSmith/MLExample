# Tensorflow Installation Guide:
This guide will instruct you on how to install Tensorflow on your machine using Anaconda.  The install done here is a basic install for the purposes of the tutorials in this repo.  This install will not include Tensorflow’s GPU compatibilities.  For more information, please see the Tensorflow installation page on their [website](https://www.tensorflow.org/install/).

## Step 1: Install Anaconda
The first step is to install Anaconda.  Anaconda is a python platform that works on Windows, Linux, and Mac.  It includes a python build and a wide assortment of python packages and tools.  Anaconda manages packages and makes sure the versions you have installed are all compatible with each other.  It also allows you to create virtual environments that are good for separating your libraries and preventing them from interfering with each other, allowing different versions to be installed on your machine next to each other for different purposes.  The installer for Windows and Mac can be downloaded from their [website](https://www.anaconda.com/download/), if you are using Linux, a quick google search can tell you what the preferred method of installation is for your favored distribution.

## Step 2: Setup New Anaconda Environment
In this step, you will set up a new environment that we will install Tensorflow into.  To setup the environment, open the anaconda prompt and run the following command (in Linux and Mac, this can also just be run in the terminal):

```
(base)$ conda create -n tf notebook numpy matplotlib tqdm ipywidgets h5py \
tensorflow
(base)$ source activate tf
(tf)$
```

This command will create a new environment called “tf” that has all of the packages we will need.  If you are familiar with Anaconda and wish to include other packages, feel free to do so.  The next line switches the current environment from “base” to the new “tf” environment we just created.  You can verify this by noting that the `(base)` at the beining of the command prompt has switched to `(tf)`.

And that's it!  You now have tensorflow installed on your system and are ready to get cracking!  Remember to start Jupyter to access the tutorials.
