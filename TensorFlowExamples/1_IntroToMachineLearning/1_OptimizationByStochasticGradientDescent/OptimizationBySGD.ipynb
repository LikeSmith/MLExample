{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import h5py\n",
    "from tqdm.notebook import trange"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Optimization by Stochastic Gradient Descent\n",
    "\n",
    "The fundimental pricipal behind Neural Networks and much of Machine Learning (ML) is the idea of optimization.  Optimization in its simplest terms is, given a set of parameters $\\mathbf{x}$, and a cost $c\\left(\\mathbf{x}\\right)$, finding the set of parameters that minimizes the cost.  The key do doing this is finding when $\\frac{dc}{d\\mathbf{x}}=0$.  There is a vast body of work on different approaches to doing this under many different constraints and conditions, but for ML, we focus on a method call Stochastic Gradient Descent (SGD).  In this tutorial, we will illustrate the idea of Stochastic Gradient Descent with a simple curve fitting example.\n",
    "\n",
    "## Stochastic Gradient Descent\n",
    "To define SGD, we will expand our problem definition. Given a set of data points $\\left(\\mathbf{x}_i, \\mathbf{y}_i\\right) \\in D$, a paramatarized model $f\\left(\\mathbf{x}\\vert\\mathbf{p}\\right)$ where $\\mathbf{p}$ are the trainable parameters, we want to find the set of parameters that minimize a cost function $c\\left(f\\left(\\mathbf{x}_i\\vert\\mathbf{p}\\right), \\mathbf{y}_i\\right) \\forall \\left(\\mathbf{x}_i, \\mathbf{y}_i\\right) \\in D$.  SGD is a numerical solution to this problem that follows the gradient of the cost to a local minimum.  We start by assuming an initial estimate of $\\mathbf{p}$, then apply the fundimental equation of gradient descent:\n",
    "\\begin{equation}\n",
    "\\mathbf{p}_{i+1} = \\mathbf{p}_i - l_r\\left.\\frac{\\partial c}{\\partial \\mathbf{p}}\\right|_{\\mathbf{p}_i}\n",
    "\\end{equation}\n",
    "where $l_r > 0$ is a small number called the learning rate.  This is a small number that is used to scale the gradient.  If the learning rate is to large, the fit can overshoot the minima or even become unstable.\n",
    "\n",
    "Ideally, we would calculate the average of these gradients over the entire dataset $D$ at once, however this is often computationally not feasable.  To remedy this, we break the dataset into minibatches of data.  For each minibatch, we average the gradients in the minibatch and update the parameters, then move on to the next minibatch.  One cycle through the entire dataset is commonly reffered to as an \"epoch.\"  Training of a mmodel is often done over several epochs, the exact number depends on the complexity of the problem, the size of the dataset and many other factors.\n",
    "\n",
    "### Example\n",
    "For our example, we will use data taken from a linear, second order, underdamped mass spring damper system.  We know from linear system theory that the position of the mass can be modeled with the following function:\n",
    "\\begin{equation}\n",
    "x_p = f\\left(t \\vert K, \\tau, \\omega_d\\right) = K\\left(1 - \\exp\\left(-\\frac{t}{\\tau}\\right)\\cos\\left(t\\omega_d\\right)\\right)\n",
    "\\end{equation}\n",
    "where $K$ is the DC Gain fo the system, $\\tau>0$ is the time consant of the system, and $\\omega_d>0$ is the damped natural frequency of the system.  Thus we have a parameter vector $\\mathbf{p} = \\left[K, \\tau, \\omega_d \\right]^T$.\n",
    "\n",
    "Given a batch of $N$ data pairs $\\left(t_i, x_i\\right) \\in D$ we can fit our curve to the data using the following least squares cost function\n",
    "\\begin{equation}\n",
    "c\\left(x_p, x_i\\right) = \\left(x_i - x_p\\right)^2\n",
    "\\end{equation}\n",
    "where $x_p$ is the predicted position from the model above.  The gradient can be calulated with respect to the parameters:\n",
    "\\begin{equation}\n",
    "\\nabla c = \\frac{\\partial c}{\\partial \\mathbf{p}} = \\left[2\\frac{\\partial x_p}{\\partial K}\\left(x_i - x_p\\right)\\ 2\\frac{\\partial x_p}{\\partial \\tau}\\left(x_i - x_p\\right)\\ 2\\frac{\\partial x_p}{\\partial \\omega_d}\\left(x_i - x_p\\right)\\right]^T\n",
    "\\end{equation}\n",
    "where\n",
    "\\begin{equation}\n",
    "\\frac{\\partial x_p}{\\partial K} = \\frac{\\partial f}{\\partial K} = \\left(1 - \\exp\\left(-\\frac{t}{\\tau}\\right)\\cos\\left(t\\omega_d\\right)\\right)\n",
    "\\end{equation}\n",
    "\\begin{equation}\n",
    "\\frac{\\partial x_p}{\\partial \\tau} = \\frac{\\partial f}{\\partial \\tau} = -K\\frac{t}{\\tau^2}\\exp\\left(-\\frac{t}{\\tau}\\right)\\cos\\left(t\\omega_d\\right)\n",
    "\\end{equation}\n",
    "\\begin{equation}\n",
    "\\frac{\\partial x_p}{\\partial \\omega_d} = \\frac{\\partial f}{\\partial \\omega_d} = K\\omega_d\\exp\\left(-\\frac{t}{\\tau}\\right)\\sin\\left(t\\omega_d\\right)\n",
    "\\end{equation}\n",
    "\n",
    "Given these calculations, we can use the stochastic gradient descent algorithm described above to perform our optimization.\n",
    "\n",
    "First, lets load some data and look at what we are working with.  The data has been included with this repo as a hdf5 file.  These files are a convienient way to store datasets, this file contains 3 datasets, a clean one, and two with noise.  For this exercise, we will be using the clean dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load data\n",
    "with h5py.File(\"../../../Data/MSDResponse/MSDResponse.h5\", \"r\") as f:\n",
    "    t = f[\"/clean/t\"][:]\n",
    "    x = f[\"/clean/x\"][:]\n",
    "    \n",
    "plt.figure()\n",
    "plt.plot(t, x, 'b.')\n",
    "plt.xlabel(\"time (s)\")\n",
    "plt.ylabel(\"mass position (m)\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see from this plot that data is a nice, clean curve.  To try with different curvs, change `clean` to `noise1` or `noise2` in the two lines that load the time and postion data from the hdf5 file.\n",
    "\n",
    "Next we need to define the model we are trying to learn and as well as all the gradient calculations.  To do this, we will define a seriese fo functions that represent the calculations we derrived above.  For the cost function, and it's gradient, we will set them to calculate on a minibatch.  This is done by passing the minibatch to the function as an array of data were each row is one dataset.  This results in the first dimenstion of the array acting as the \"batch dimension\" and the data being represented by row vectors.  This is the standard way to represent data in tensorflow and many other ML tools."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define function to fit\n",
    "def f(t, p):\n",
    "    K = p[0]\n",
    "    tau = p[1]\n",
    "    w_d = p[2]\n",
    "    \n",
    "    return K*(1 - np.exp(-t/tau)*np.cos(t*w_d))\n",
    "\n",
    "# Define cost\n",
    "def c(T, X, p):\n",
    "    c = 0\n",
    "    for i in range(T.shape[0]):\n",
    "        c += (X[i] - f(T[i], p))**2\n",
    "    return c / T.shape[0]\n",
    "\n",
    "# Define Gradients\n",
    "def f_grad(t, p):\n",
    "    K = p[0]\n",
    "    tau = p[1]\n",
    "    w_d = p[2]\n",
    "    \n",
    "    x_K =  (1 - np.exp(-t/tau)*np.cos(t*w_d))\n",
    "    x_tau = -K*t/(tau**2)*np.exp(-t/tau)*np.cos(t*w_d)\n",
    "    x_w_d = K*w_d*np.exp(-t/tau)*np.sin(t*w_d)\n",
    "    \n",
    "    return np.array([x_K, x_tau, x_w_d])\n",
    "\n",
    "def c_grad(T, X, p):\n",
    "    grad_c = np.zeros(3)\n",
    "    for i in range(T.shape[0]):\n",
    "        grad_c += -2*f_grad(T[i], p)*(X[i] - f(T[i], p))\n",
    "    return grad_c / T.shape[0]"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have defined our problem, we can now impliment the SGD algorithm.  To do this, we need to define our minibatch size, the number of epochs we want to train for, and the learning rate.  We also need to make our inital guess at the parameters, we can do this by inspecting the data plotted above to make rough estimates. The DC Gain is going to be on the order of 1 (it will probably be about 5), the time constant will be on the order of 0.1, and the frequency will be on the order of 10.  We will use these values as out initial guesses for the parameters.  We will then impliment the SGD algorithm as described above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Training Parameters\n",
    "epochs = 30\n",
    "batch_size = 32\n",
    "lr = 0.01\n",
    "p_0 = [1, 0.1, 10]\n",
    "\n",
    "# Training\n",
    "costs = np.zeros(epochs)\n",
    "p = np.zeros((epochs+1, len(p_0)))\n",
    "p[0, :] = p_0\n",
    "\n",
    "n_batches = int(np.ceil(t.shape[0]/batch_size))\n",
    "\n",
    "for epoch in trange(epochs):\n",
    "    perm = np.random.permutation(t.shape[0])\n",
    "    p_i = p[epoch, :]\n",
    "    \n",
    "    for batch in trange(n_batches):\n",
    "        T_batch = t[perm[batch*batch_size:min((batch+1)*batch_size, t.shape[0])]]\n",
    "        X_batch = x[perm[batch*batch_size:min((batch+1)*batch_size, x.shape[0])]]\n",
    "        costs[epoch] += c(T_batch, X_batch, p_i)\n",
    "        grad = c_grad(T_batch, X_batch, p_i)\n",
    "        p_i -= lr*grad\n",
    "    \n",
    "    p[epoch+1, :] = p_i"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the above block, we initialize all our values and set up arrays to track the cost and keep track of the parameter values during training.  We then impliment the SGD algorithm in a for loop.  First, we get the current batch of data based on a random permutation of the indicies.  This shuffles our datasets and mixes up the data as it is trained.  We then calculate the cost and gradient, and finally update the cost.  At the end of each epoch, we store the current estimate of the parameters.\n",
    "\n",
    "Now that the training is complete, lets examine the results.  We can look at the resutls.  lets first plot the predicted model on top of the data used to train it.  We will then plot the cost throughtout the training, and the values of the parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.plot(t, x, 'b.')\n",
    "plt.plot(t, f(t, p[-1, :]), 'r')\n",
    "plt.xlabel('time (s)')\n",
    "plt.ylabel('displacement')\n",
    "plt.legend(('data', 'fitted curve'))\n",
    "\n",
    "plt.figure()\n",
    "plt.plot(costs)\n",
    "plt.xlabel('epoch')\n",
    "plt.ylabel('cost')\n",
    "\n",
    "plt.figure()\n",
    "plt.plot(p[:, 0])\n",
    "plt.plot(p[:, 1])\n",
    "plt.plot(p[:, 2])\n",
    "plt.xlabel('iteration')\n",
    "plt.legend(('K estimate', 'tau estimate', 'omega estimate'))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also see that the learning has converged as the cost and the estimates have stopped changing significantly.  This is an indication that we are near the local minimum.  We can see that the overall fit is pretty good and the model closely matches the data.\n",
    "\n",
    "### Pitfalls\n",
    "When using the SGD algorithm, there are several pitfalls to be aware of.  Consider a system with a single parameter who's cost surface is given in this figure: ![local_extrema](figs/local_extrema.png) The red dots are places where the gradient of this surface is zero, also known as the functions extrema.  Note the one in the middle is not a minimum, but a maximum point.  If the initial guess at the parameter were set to this value, then the gradient descent algorightm would not work because the gradient is zero at that point.  Fortunately, for this to be an issue, the initial guess must be exactly on the maximum.  If the estimate is even slightly off of that point, then the gradient descent algorightm will move away from it.\n",
    "\n",
    "More concerning for us is the fact that this function aslo has two minima points.  These are referred to as \"Local Minima,\" or points that hold the smallest value for a range that is a subset of the entire range of the function.  Depending on where we choose our initial guess, we will arrive at one of these minima.  If we choose to start at -0.1, the gradient descent will take us down the gradient toward the minimum on the left, while if we start at 0.1, the gradient will descent toward the minimum on the right.  Once we arrive at a minimum, we only know that it is a local minimum, we need other tools and knowledge to determine if it is the \"Global Minimum\" or the the smallest value for the entire range of the function.  To counter this, many machine learning algorithms use a technique called Exploration.  Exploration just means sometimes sampeling other random parts of the space just to see well it works and to make sure it is not better than our current solution.  For example, a genetic algorithm would pick several starting points and evaluate the cost at each of these.  It would then eliminate a fraction of the sample points and resample these points randomely near the points it kept.  This process starts by examing a wide area, but will converge to local minima.  The values of these minima can then be evaluated to see which is the Global Minimum.  For simple curve fitting like we have done in this tutorial, this makes choosing the correct local minimum a critical part of the problem.  However with Neural Networks, which we will be exploreing in the next tutorial, most local minima are also the Global Minimum, and the larger the network, the less likely a local minimum is to be a sub-optimal minimum.  See [this paper](https://arxiv.org/abs/1412.0233) for details.\n",
    "\n",
    "### On Your Own\n",
    "Now it is time for you to play with this example a bit.  you can either modify the code directly in this notebook, or you can copy it to a python file and edit it in your editor or python IDE of choice, just make sure to point it to the anaconda environment you created for this tutorial and to adjust any paths accordingly. \n",
    "\n",
    "1. Adjust the initial guess and training parameters, how do the effect the fit?\n",
    "    1. What parameter is the algorithm most robust to?\n",
    "    2. Which is it least robust to?\n",
    "    3. What happens when the learning rate gets large?\n",
    "    4. What about when it gets small?\n",
    "    5. How does the batch size influence the learning?\n",
    "    6. What about he number of epochs being trained on?\n",
    "2. Impliment a more intelligent stop condition and just training for a number of epochs.\n",
    "    1. What are we looking for to determin that the training is done?\n",
    "    2. How can we impliment such a check programatically?\n",
    "3. What if we have less clean data?  Load the noisier datasets, are we still able to learn?\n",
    "    1. How do wee need to adjust the training hyperparameters (learning rate, number of epochs, batch size, etc) to learn on noisy data?\n",
    "    2. What does learning on noisy data do to the final cost?\n",
    "    "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
