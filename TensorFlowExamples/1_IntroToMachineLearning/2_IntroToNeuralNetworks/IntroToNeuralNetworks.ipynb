{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import tensorflow as tf\n",
    "import numpy as np\n",
    "from tqdm.notebook import trange\n",
    "import h5py\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Neural Networks\n",
    "If you have found this tutorial, you have most likely heard of Neural Networks (NNs) before, but what is it exactly?  Truns out, all an NN is, is a heavily parameterized function approximator.  Like the model we used in the previous tutorial, an NN approximates a class of functions by varying a set of parameters.  The difference is that an NN can approximate a much wider set of functions, but it also has a lot more parameters.  For problems such as the non-linear curve fit we did before where we have a good idea what the shape of the curve is a priori, using these simpler models is better.  NNs are often used for more abstract functions that are more difficult to define, such as the problem we will be considering in this tutorial: determining what integer is contained in an image.\n",
    "\n",
    "Structurally, a neural network is comprized of layers of neurons where each neuron contains a single number.  Neurons are connected to eachother, each connection has a weight associated with it.  The value in a neuron is calculated by summing the values of the neurons connected to it multiplied by the weights of the connections plus a bias.  Once this value is calculated, a non-linear activation function is applied and the resulting value is stored in the neuron.  The following diagram is a type of NN known as a MultiLayered Perceptron (MLP).  ![NN_diagram.png](figs/NN_diagram.png)  It consists of an Input Layer (blue) where each neuron stores the value of an input.  An Output layer (green) where each neuron contains a value of the output.  And at least one hidden layer between them.  These layers are arranged in a sequence, and each layer is densely connected to the layer before it.  This means the each neuron is connected to all of the neurons in the previous layer.  These densely connected layers can be written out mathematically as matrix multiplication if we view the values of each layer as a vector:\n",
    "\\begin{equation}\n",
    "\\mathbf{h}_{i+1} = \\sigma_i\\left(W_i\\mathbf{h}_i + b_i\\right)\n",
    "\\end{equation}\n",
    "where $W_i$ is a matrix of the weights connecting layers $i$ and $i+1$, $b_i$ are the biases, $\\mathbf{h}_i$ is the vector containg the values of layer $i$, and $sigma$ is a non-linear activation function that is applied element-wize to the vector.  Note that each layer has a different size.  The larger the hidden layers, the more flexible the Neural Network is and the capacity it has.  We can write out the equation for an MLP with a single hidden layer as\n",
    "\\begin{equation}\n",
    "\\mathbf{o} = \\sigma_2\\left(W_2*\\sigma_1\\left(W_1*\\mathbf{i}+b_1\\right)+b_2\\right) = f\\left(\\mathbf{i}\\ \\vert\\ W_1, W_2, b_1, b_2\\right)\n",
    "\\end{equation}\n",
    "Note that we can parameterize the NN with the weights and biases.  This NN can approximate any continuous function, assuming the hidden layer is large enough.  However, it is often prudent to use different NN structures and multiple layers as you will see later in these tutorials to get better approximation.\n",
    "\n",
    "## Activation Functions\n",
    "We have mentioned the non-linear activation functions but have neglected to describe them in detail until now.  This is the key part of NNs that makes them more than just linear approximation.  Consider the MLP with one hidden layer.  Without the activation function, the expression for the NN can be written as\n",
    "\\begin{equation}\n",
    "\\mathbf{o} = W_2\\left(W_1\\mathbf{i} + b_1\\right) + b_2\n",
    "\\end{equation}\n",
    "But this can be simplified to\n",
    "\\begin{equation}\n",
    "\\mathbf{o} = W_3\\mathbf{i} + b_3\n",
    "\\end{equation}\n",
    "where $W_3 = W_2W_1$ and $b_3 = W_2b_1 + b_2$ and thus adding a hidden layer does not add any functionality to the NN.  However, when we use non-linear activation functions, this simplification is not valid, and thus the NN becomes much more flexible.  While any non-linear function can be used as an activation function so long as it is differentiable, there are a few popular ones.  The logistic function, known in ML circles as the \"sigmoid\" function was very popular in the early days of neural networks, as the resulting activation is between 0 and 1.  Later, the hyperbolic tangent became popular as it allowed neurons to hold negative values as well.  Today, the REctified Linear Unit (ReLU) ($\\text{relu}\\left(x\\right) = \\text{max}\\left\\lbrace x, 0 \\right\\rbrace$) is very popular as it solves some problems associeted with sigmoid and tanh where their derivatives go to 0 away for large values.  There are many others that are used in some specific applications such as softmax, which we will use later in this tutorial.\n",
    "\n",
    "## Training\n",
    "We have discussed the structure of NNs, now we will discuss how they are trained. There have been many methods proposed, however by far the most common method used today is error backpropogation with Stochastic Gradient Descent (SGD) which we discussed in the previous tutorial.  NNs are trained the same way we solved for the non-linear model, just much larger parameter spaces.  Consider the MLP from before.  An MLP model is parameterized by the weights in $W_1$, $W_2$, $b_1$, and $b_2$.  We can calculate the gradients of a loss function with respect to these parameters to propogate the loss back through the NN, this process is known as error backpropigation, and SGD is the most common algorithm used to do this.\n",
    "\n",
    "When training NNs, we will often take a small chunck of the training data and set it asside.  This is called the validation set.  The NN will be trained on the rest of the data, and after the NN does one pass of the training data, we evaluate the average loss over the validation set.  However, we DO NOT train the NN on this data.  By comparing this loss to the average loss during training, we can make inferences about the quality of the learning.  For example, if the average loss is decreasing, and so is the validation, the NN is learning well.  It is learning patterns in the data that it can generalize to data it has not seen during training.  However, if the avarage loss is decreasing and the validation is increasing, or staying level, that means the NN is now learning patterns specific to the training data that do not generalize to other data.  We call this memorization.  Often, NNs will start off learning well, but will start to memorize after a while.  We can use this to indicate that the training should end.  This technique is known as early stopping.\n",
    "\n",
    "## The Black Magic of Neural Networks\n",
    "So far, we have discussed how to build NNs, and how to train them in a general sense, and you may be saying to yourself, Awesome! Makes sense! Now how do I design a NN to solve the particular problem?  And therein lies the trick.  You may have notice in the discussion above, we left out some key points like how big should hidden layers be?  What activation functions should I use?  What should I use as my loss function?  All of these are excellent questions to which the answer is...no one really knows.  The structure of NNs is still a very active area of research.  We know that as NNs get deeper, the representations of the data in layers tend to become more abstract.  We know adding neurons increases the capacity of the NN, but also increases the amount of training data needed ot succesfully train the NN.  These values such as number of layers, size of layers, activation functions, etc. are known as hyperparameters of the NN, and often must just be guessed.  There are some methods of exploring the hyperparameter space, but these are often computationally very expensive and involve training the network many times.\n",
    "\n",
    "Another area of difficulty when it comes to NNs is the data it is trained on.  This data is all the NN knows and so it is very sensitive to it.  When training, make sure your data is clean, curated, and unbiased...otherwise you may, for example, train a chat bot on random tweets and have it come our racist. Consider the data and what underlying patterns and biases may exist that you do not intend for your NN to learn.  This goes with the cost function as well.  Make sure there are not unexpected local minima that can be run into.  To compare different NNs trained on the same dataset, we will often set asside another small portion of the data referred to as a test dataset that is not used for training or validation.  This allows us to make judgments on the performance of the NNs on data that never saw durring the training phase, either for training or validation.\n",
    "\n",
    "Much of the knowledge of how to do this right now comes from experience.  Build, train, and play with your NNs and see how the behave.  You will develop and intuition for this \"Black Magic.\"  Until we can figure out more about NNs, this is the best we can do.\n",
    "\n",
    "## TensorFlow\n",
    "We have talked a lot about NNs, but how do we actually build them?  We could always use tools like numpy in python, or MatLab that make doing linear algebra easy, but these tools tend to be very slow, and for the amount of data that needs to be processed in training an NN, suboptimal code can add a lot of processing time.  Faster languages like C/C++ are more difficult to impliment a lot of the math in and do not handle linear algebra natively.  At its base, TensorFlow is a library developed by Google that allows you to specify the computations you want to make.  It produces an optimized compute graph that is executed in fast C code when you go to run it.  It also automatically calculates gradients for you using automatic differentiation so all you have to do is specify the forward propogation of your network and the backwards gradients are automatically calculated.\n",
    "\n",
    "In addition to this low level functionallity, TensorFlow also provides many higher level interfaces for building networks faster and easier.  For example, there are many built in functions that automatically generate various layers by generating the appropriate weights and applying them to an input.  An example of this would be the dense layer which is the standard neural network layer described above.  TensorFlow also has a number of other layers such as convolutional layers and recurrent layers built in.  TensorFlow also provides some common optimizers such as RMSProp and Adam.  These are specialized SGD algorithms that work well for training neural networks.  The built in optimizers can be passed the loss tensor and will automatially calculate and apply gradients.\n",
    "\n",
    "For more information, see their [website](www.tensorflow.org).\n",
    "\n",
    "## MNIST Integer Classification\n",
    "\n",
    "It is time to build an example Neural Network!  The MNIST data set is a curated set of images of hand drawn numerals.  The following are samples from the dataset: ![MNIST_Images.png](figs/MNIST_Images.png) A classic problem is to train an ML model that is capable of classifying the images as whatever number is represented.  The first thing we need to is load the dataset. This dataset has a total of 70,000 datapoints.  Each datapoint has two parts, a grayscale image, and a label specifing the number drawn in the image. The dataset is available [here](http://yann.lecun.com/exdb/mnist/) and is included in this repo in an hdf5 file.  For the labels, we use a \"one hot\" representation.  A one hot label is an array of ten numbers where the number corresponding to the label is 1 and the rest are 0.  For example, 1 would be $\\left[0, 1, 0, 0, 0, 0, 0, 0, 0, 0\\right]$ or 5 would be $\\left[0, 0, 0, 0, 0, 1, 0, 0, 0, 0\\right].  One hot representation is often used in classification problems as each column can represent a class, and the output of the Neural Network will be a set of probabilities that the input is a part of the various classes.\n",
    "\n",
    "In this next block, we load the training data from the provided hdf5 file.  The dataset is already broken into "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with h5py.File(\"../../../Data/MNIST/mnist.h5\", \"r\") as f:\n",
    "    train_imgs = f[\"train/img\"][:, ...]\n",
    "    train_labs = f[\"train/lab1h\"][:, ...]\n",
    "    valid_imgs = f[\"valid/img\"][:, ...]\n",
    "    valid_labs = f[\"valid/lab1h\"][:, ...]\n",
    "    test_imgs = f[\"test/img\"][:, ...]\n",
    "    test_labs = f[\"test/lab1h\"][:, ...]\n",
    "    \n",
    "img_size = train_imgs.shape[1:]"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Build The Network\n",
    "\n",
    "For this problem, we will be using a MultiLayer Perceptron (MLP) with two hidden layers of size 128 and 32.  Before we apply the MLP, we will flatten the image from a 2D tensor to a 1D vector.  We will then use the two hidden layers with ReLu activation.  Finally, we will add the output layer which is also densly connected to the second hidden layer with softmax activation.  Softmax ephasises the maximum value in a vector, tending it towards 1, while minimizing all the other elements close to 0.  It is often used as the output activation for classification networks as it will emphasise which case the classifier things the input is.\n",
    "\\begin{equation}\n",
    "Softmax\\left(\\mathbf{z}\\right) = \\frac{exp\\left(\\mathbf{z}\\right)}{\\sum^K_{j=1}exp\\left(z_j\\right)}\n",
    "\\end{equation}\n",
    "where $z_i$ are the $i$th elements of $\\mathbf{z}$ respectively, and $K$ is the number of elements in $\\mathbf{z}$.\n",
    "\n",
    "In this tutorial, we will be doing a lot of the math by hand and just using tensorflow to generate the weights.  In the next tutorial, we will look at some of the higher level tensorflow APIs that will do majority of this work automatically, allowing you to focus on designing the NN rather than implimenting it.\n",
    "\n",
    "To start, we will define our hidden layer sizes, as well as the learning rate.  We then need to create 2 wieghts for each of our 3 layers (2 hidden layers and an output layer).  For the weights, we will initialize them using a uniform random distribution with the limits being a function of the size of the matrix.  This is called a [Glorot](http://proceedings.mlr.press/v9/glorot10a.html) initialization.  For the biases, we will just initialize them to 0.  Finally, we will package of of our training weights into a list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "h1_size = 128 # first hidden layer size\n",
    "h2_size = 32  # second hidden layer size\n",
    "\n",
    "# Create Weight Tensors\n",
    "limit = np.sqrt(6/(h1_size + np.prod(img_size)))\n",
    "W_1 = tf.Variable(np.random.uniform(-limit, limit, (np.prod(img_size), h1_size)), dtype=\"float32\")\n",
    "b_1 = tf.Variable(np.zeros(h1_size), dtype=\"float32\")\n",
    "\n",
    "limit = np.sqrt(6/(h2_size + h1_size))\n",
    "W_2 = tf.Variable(np.random.uniform(-limit, limit, (h1_size, h2_size)), dtype=\"float32\")\n",
    "b_2 = tf.Variable(np.zeros(h2_size), dtype=\"float32\")\n",
    "\n",
    "limit = np.sqrt(6/(10 + h2_size))\n",
    "W_3 = tf.Variable(np.random.uniform(-limit, limit, (h2_size, 10)), dtype=\"float32\")\n",
    "b_3 = tf.Variable(np.zeros(10), dtype=\"float32\")\n",
    "\n",
    "weights = [W_1, b_1, W_2, b_2, W_3, b_3]"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Train the Network\n",
    "Now we can train the network!  We will use the same SGD algorithm we used in the previous tutorial, however we will be using tensorflow to calculate the gradients.  We will not track the individual weight values through training, however we will also impliment a validation test at the end of each epoch.  For the loss function, we will use the Mean Squared Error (MSE) between the desired output, and the expected output."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n_epochs = 100\n",
    "batch_size = 128\n",
    "l_r = 0.02\n",
    "\n",
    "n_batches = int(np.ceil(train_imgs.shape[0] / batch_size))\n",
    "trn_losses = np.zeros(n_epochs)\n",
    "val_losses = np.zeros(n_epochs)\n",
    "\n",
    "epoch_bar = trange(n_epochs)\n",
    "epoch_bar.set_description(\"Validation Loss: N/A\")\n",
    "\n",
    "for epoch in epoch_bar:\n",
    "    # Shuffle data\n",
    "    perm = np.random.permutation(train_imgs.shape[0])\n",
    "    \n",
    "    batch_bar = trange(n_batches)\n",
    "    batch_bar.set_description(\"Training Loss: N/A\")\n",
    "    for batch in batch_bar:\n",
    "        # Get batch\n",
    "        imgs = train_imgs[perm[batch*batch_size:min((batch+1)*batch_size, train_imgs.shape[0])], :, :]\n",
    "        labs = train_labs[perm[batch*batch_size:min((batch+1)*batch_size, train_labs.shape[0])], :]\n",
    "        \n",
    "        # Make prediction and calculate Loss\n",
    "        with tf.GradientTape() as tape:\n",
    "            for w in weights:\n",
    "                tape.watch(w)\n",
    "                \n",
    "            x = tf.reshape(imgs, (imgs.shape[0], np.prod(img_size)))\n",
    "            x = tf.nn.relu(tf.matmul(x, W_1) + b_1)\n",
    "            x = tf.nn.relu(tf.matmul(x, W_2) + b_2)\n",
    "            x = tf.nn.softmax(tf.matmul(x, W_3) + b_3)\n",
    "            loss = tf.reduce_mean(tf.math.squared_difference(x, labs))\n",
    "        \n",
    "        # Calculate and apply gradients\n",
    "        grads = tape.gradient(loss, weights)\n",
    "        for g, w in zip(grads, weights):\n",
    "            w.assign_add(-l_r*g)\n",
    "        \n",
    "        # Log loss\n",
    "        trn_losses[epoch] += loss.numpy()\n",
    "        batch_bar.set_description(\"Training Loss: %f\" % (trn_losses[epoch] / (batch + 1),))\n",
    "    \n",
    "    # Calculate validation loss\n",
    "    x = tf.reshape(valid_imgs, (valid_imgs.shape[0], np.prod(img_size)))\n",
    "    x = tf.nn.relu(tf.matmul(x, W_1) + b_1)\n",
    "    x = tf.nn.relu(tf.matmul(x, W_2) + b_2)\n",
    "    x = tf.nn.softmax(tf.matmul(x, W_3) + b_3)\n",
    "    loss = tf.reduce_mean(tf.math.squared_difference(x, valid_labs))\n",
    "    \n",
    "    # Log losses\n",
    "    trn_losses[epoch] /= n_batches\n",
    "    val_losses[epoch] = loss.numpy()\n",
    "    \n",
    "    epoch_bar.set_description(\"Validation Loss: %f\" % (val_losses[epoch],))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluate Results\n",
    "Now that we are done training, we can evaluate some of the results.  First we will examine the training history by plotting the training and validation loss:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.plot(trn_losses)\n",
    "plt.plot(val_losses)\n",
    "plt.xlabel(\"epoch\")\n",
    "plt.ylabel(\"loss\")\n",
    "plt.legend([\"Training Loss\", \"Validation Loss\"])"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is common for the validation lot to be better than the training loss, especially at the beginning.  This is because the the validation loss is calculated with the final weights of th epoch, where the training loss is the average of the training loss during the epoch, including the loss at the beginning, when, theoretically, the loss is greater since it should be shrinking during the training.  However as the NN approaches the local minimum, the training loss will become the smaller value as the changes in the overall loss with each step decreases.  We would expect the validation loss to be higher than the loss calculated on the training data given the same weights because the weights were not trained on the validation set.  Another common trend is that the validation loss may start to increase.  This is indicative of the NN starting to memorize the training data rather than learning innovations that generally improve its performance.\n",
    "\n",
    "Now we can evaluate the performance using the test dataset.  We will first calculate the test loss.  This value can be used to compare this NN to others if we change hyperparameters, such as layer sizes, learning rate, etc.  We will then plot the image the NN is most confident in for each numeral.  Finally we will find the image with the highest entropy in its prediction (the image the NN had the hardest time classifying) and plot it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Apply NN to test dataset\n",
    "x = tf.reshape(test_imgs, (test_imgs.shape[0], np.prod(img_size)))\n",
    "x = tf.nn.relu(tf.matmul(x, W_1) + b_1)\n",
    "x = tf.nn.relu(tf.matmul(x, W_2) + b_2)\n",
    "pred = tf.nn.softmax(tf.matmul(x, W_3) + b_3)\n",
    "loss = tf.reduce_mean(tf.math.squared_difference(pred, test_labs))\n",
    "\n",
    "print(\"Test Loss: %f\" % (loss.numpy(),))\n",
    "\n",
    "# Get image with highest confidence for each numeral\n",
    "best = np.argmax(pred, axis=0)\n",
    "\n",
    "plt.figure()\n",
    "for i in range(10):\n",
    "    plt.subplot(2, 5, i+1)\n",
    "    plt.imshow(test_imgs[best[i], :, :], cmap=\"gray\")\n",
    "    plt.title(\"Pred: %d\" % (i,))\n",
    "    \n",
    "# Find hardest image to classify\n",
    "entropy = -np.sum(pred.numpy()*np.log(pred.numpy()), axis=1)\n",
    "hardest = np.argmax(entropy)\n",
    "plt.figure()\n",
    "plt.imshow(test_imgs[hardest, :, :], cmap=\"gray\")\n",
    "plt.title(\"Pred: %d (?)\" % (np.argmax(pred[hardest, :]),))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that there are examples where the NN predicted correctly with high confidence.  However there are also examples of low confidence predictions.\n",
    "\n",
    "## On Your Own\n",
    "We have given a basic implimentation of a simple neural network, now it is time to play with this example.  You can modify this notebook, or copy the code into your own python file.\n",
    "\n",
    "1. Further results analysis\n",
    "    1. What is the overall successfull classification rate on the test dataset?\n",
    "    2. What is the success rate for each numeral?  Are some numerals harder to classify?\n",
    "    3. What percentage of the wrong predictions also have a high confidence?\n",
    "2. Improve Performance:\n",
    "    1. What happens if you change the layer sizes?\n",
    "    2. What about the learning rate or the number of training epochs?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
