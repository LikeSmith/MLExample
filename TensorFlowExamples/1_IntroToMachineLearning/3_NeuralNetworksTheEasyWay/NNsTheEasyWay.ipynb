{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import tensorflow as tf\n",
    "import numpy as np\n",
    "import h5py\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Neural Networks the Easy Way\n",
    "In the previous tutorial, we built a simple Neural Network (NN) from scratch, only using some base tensorflow APIs to calculate the gradients during training.  However, when doing Machine Learning (ML) using tensorflow, you often do not have to muck around in the basic tensorflow APIs, as much of the work we did is done automatically through some of the higher level APIs.  Specifically, Keras is a library built ontop of tensorflow that provides a high level interface that makes it easy to quickly set up and train simple networks.  And because it is intergrated into tensorflow, it is easy to impliment many non-standard features with only a little extra effort.\n",
    "\n",
    "## Building a Keras Model\n",
    "The simplest way to build a keras model is using their sequential model.  In this case, you simply provide a list of layers that are applied in a sequence.  In this tutorial, we will use Dense layers which represent the kind of densely connected layers we implemented by hand in the previous tutorial. We will also use a flatten layer to convert the image into a 1-dimentional vector.  Using keras, the training weights will automatically be created and tracked.  We will create a sequential keras model that is the same as the one we set up in the previous tutorial consisting of 3 dense layers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mdl = tf.keras.Sequential(\n",
    "    [\n",
    "        tf.keras.layers.Flatten(),\n",
    "        tf.keras.layers.Dense(units=128, activation=\"relu\", name=\"layer_1\"),\n",
    "        tf.keras.layers.Dense(units=32, activation=\"relu\", name=\"layer_2\"),\n",
    "        tf.keras.layers.Dense(units=10, activation=\"softmax\", name=\"layer_3\")\n",
    "    ]\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "now that we have a model, we can compile it with a loss function and an optimizer.  Keras has several built in loss functions, including the Mean Squared Error (MSE) loss function we used in the previous tutorial.  There are also several optimizers we can use.  These optimizer impliment different variations of the Sotchastic Gradient Descent (SGD) algorithm we have been using.  The most basic algorithm provided is a base SGD with momentum, a slightly more complicated version of the SGD algorithm we are using, however we can set the momentum value to 0 to get the basic SGD algorithm.  We will be using the basic SGD algorithm to maintain compareability to the previous tutorials, however more complex algorithms such as RMSProp, Adam, and AdaGrad are more the industry standard these days.  We will discuss these more at the end of this tutorial.  We will compile the model, that is to tell the model what loss and optimization functions to use.  We will also build the model, that is tell the model what shape the input will be, and allow it to generate all the weights."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mdl.compile(\n",
    "    loss=\"mse\",\n",
    "    optimizer=tf.keras.optimizers.SGD(learning_rate=0.01, momentum=0.0)\n",
    ")\n",
    "mdl.build((None, 28, 28))\n",
    "mdl.summary()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Training a Keras Model\n",
    "Now that we have a compiled keras model, we can train it!  This is done through the `fit` function of the model object.  This function is passed the data to train on, and handels the training loop and reporting, and returns a history object with all the relevant training history."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load Data:\n",
    "with h5py.File(\"../../../Data/MNIST/mnist.h5\", \"r\") as f:\n",
    "    train_imgs = f[\"train/img\"][:, ...]\n",
    "    train_labs = f[\"train/lab1h\"][:, ...]\n",
    "    valid_imgs = f[\"valid/img\"][:, ...]\n",
    "    valid_labs = f[\"valid/lab1h\"][:, ...]\n",
    "    test_imgs = f[\"test/img\"][:, ...]\n",
    "    test_labs = f[\"test/lab1h\"][:, ...]\n",
    "\n",
    "# Train Model\n",
    "hist = mdl.fit(\n",
    "    train_imgs,\n",
    "    train_labs,\n",
    "    validation_data=(valid_imgs, valid_labs),\n",
    "    epochs=100,\n",
    "    batch_size=128,\n",
    "    verbose=1\n",
    ")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The hist object contains training history data.  Specifically it has a field called \"history\" that is a dictionary of all the training metrics with the names as the keys.  There are two versions, one is the same name as the metric, and the other has \"val_\" tacked on to the front.  These are the traning and validation metrics respectively.  We can plot the training history similar to how we did in the previous tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.plot(hist.history[\"loss\"])\n",
    "plt.plot(hist.history[\"val_loss\"])\n",
    "plt.xlabel(\"epoch\")\n",
    "plt.ylabel(\"loss\")\n",
    "plt.legend([\"Training Loss\", \"Validation Loss\"])"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can compare this plot to the training history from the previous tutorial to see the similar performance.  However, you may have noticed that the training happened faster using the Keras model.  This is because tensorflow streamlines much of the training process and is more efficient then performing the learning by hand.\n",
    "\n",
    "We can also perform the same analysis with the test dataset that we did in the previous tutorial.  The evaluate function of the Keras model can be passed a set of inputs and labels and will return the loss.  The Keras model itself is callable, if it is passed a numpy array, it will forward propegate this through the NN and return the result. This can also be passed a tensor and be used to construct a larger network, however we will discuss this feature in more detail later."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "loss = mdl.evaluate(test_imgs, test_labs, verbose=1)\n",
    "\n",
    "pred = mdl(test_imgs)\n",
    "\n",
    "# Get image with highest confidence for each numeral\n",
    "best = np.argmax(pred.numpy(), axis=0)\n",
    "\n",
    "plt.figure()\n",
    "for i in range(10):\n",
    "    plt.subplot(2, 5, i+1)\n",
    "    plt.imshow(test_imgs[best[i], :, :], cmap=\"gray\")\n",
    "    plt.title(\"Pred: %d\" % (i,))\n",
    "    \n",
    "# Find hardest image to classify\n",
    "entropy = -np.sum(pred.numpy()*np.log(pred.numpy()), axis=1)\n",
    "hardest = np.argmax(entropy)\n",
    "plt.figure()\n",
    "plt.imshow(test_imgs[hardest, :, :], cmap=\"gray\")\n",
    "plt.title(\"Pred: %d (?)\" % (np.argmax(pred[hardest, :]),))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Advanced Keras Models\n",
    "We have recreated the simple NN we created in the privious tutorial in Keras, but there is a lot more that Keras models can do.  To showcase this, we will create another keras model to train on the MNIST dataset that will utilize some additional features of Keras.  This model will be the same shape, but rather than mse, will use sparse categorical crossentropy.  This loss function calculates the crossentropy between the predicted distribution and the actual based on the label.  Because it is a sparse loss function, it uses the number as the label rather than the one hot representation we have been using, in other words, the label for a picutre of the numeral one will be represented by 1 as an int rather than \\[0 1 0 0 0 0 0 0 0 0 0\\].  We use the Adam as the optimizer, and it will track the accuracy (percent of the samples that are labeled correctly by the NN) in addition to the loss.  While this value is not a great one for training, it is usefull for estimating how the NN is performing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Setup Model\n",
    "mdl = tf.keras.Sequential(\n",
    "    [\n",
    "        tf.keras.layers.Flatten(),\n",
    "        tf.keras.layers.Dense(units=128, activation=\"relu\", name=\"layer_1\"),\n",
    "        tf.keras.layers.Dense(units=32, activation=\"relu\", name=\"layer_2\"),\n",
    "        tf.keras.layers.Dense(units=10, activation=\"softmax\", name=\"layer_3\")\n",
    "    ]\n",
    ")\n",
    "mdl.compile(\n",
    "    loss=\"sparse_categorical_crossentropy\",\n",
    "    optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),\n",
    "    metrics=[\"accuracy\"]\n",
    ")\n",
    "mdl.build((None, 28, 28))\n",
    "mdl.summary()\n",
    "\n",
    "# Load Data:\n",
    "with h5py.File(\"../../../Data/MNIST/mnist.h5\", \"r\") as f:\n",
    "    train_imgs = f[\"train/img\"][:, ...]\n",
    "    train_labs = f[\"train/lab\"][:, ...]\n",
    "    valid_imgs = f[\"valid/img\"][:, ...]\n",
    "    valid_labs = f[\"valid/lab\"][:, ...]\n",
    "    test_imgs = f[\"test/img\"][:, ...]\n",
    "    test_labs = f[\"test/lab\"][:, ...]\n",
    "\n",
    "# Train Model\n",
    "hist = mdl.fit(\n",
    "    train_imgs,\n",
    "    train_labs,\n",
    "    validation_data=(valid_imgs, valid_labs),\n",
    "    epochs=20,\n",
    "    batch_size=128,\n",
    "    verbose=1\n",
    ")\n",
    "\n",
    "# Plot training history\n",
    "plt.figure()\n",
    "plt.plot(hist.history[\"loss\"])\n",
    "plt.plot(hist.history[\"val_loss\"])\n",
    "plt.xlabel(\"epoch\")\n",
    "plt.ylabel(\"loss\")\n",
    "plt.legend([\"Training\", \"Validation\"])\n",
    "\n",
    "plt.figure()\n",
    "plt.plot(hist.history[\"accuracy\"])\n",
    "plt.plot(hist.history[\"val_accuracy\"])\n",
    "plt.xlabel(\"epoch\")\n",
    "plt.ylabel(\"accuracy\")\n",
    "plt.legend([\"Training\", \"Validation\"])"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice how much quicker this NN trains than the previous one.  We can see that this NN started to memorize the training set after about 8 or so epochs by looking at how the validation loss trends upwards.  We can compare the performance of this NN to the previous one by evaluating it on the test dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "loss = mdl.evaluate(test_imgs, test_labs, verbose=1)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we can see an example of why a test set is important.  While the perfomance of this NN on the training set is greatly superior to that of the previous model, it's performance on the test set is not.  This indicative that this new model has memorized the training set, but does not generalize that data as well.  However, this model managed to do so in 20 epochs as opposed to the 100 that the previous NN was trained on.  Tweaking hyperperamers would be the next step here to optimize the training.\n",
    "\n",
    "## On Your Own\n",
    "Now that we have given some examples of setting up models with Keras, you can set up and train some of your own.  As with the previous tutorials, you can either modify the code here or create your own python files.\n",
    "\n",
    "1. Do the similar analysis as we did in the previous tutorial on the new model, how does the advanced keras model compare to the one we created last time?\n",
    "2. Build your own model\n",
    "    1. Play with the structure, use different layers with different sizes\n",
    "    2. Play with the optimization, use Adagrad or RMSProp, how doe these effect the performance?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
