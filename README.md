# MLExample
Example code and tutorials for Machine Learning applications

This repo is organized based on what libraries the tutorials are written to support.
Currently we are only writing tutorials for tensorflow, but we plan to add pyTorch tutorials in the future.
It is recommended to go through the tutorials in the order they appear here.
We recommend going through all the tutorials for one library before moving on to the others.

## Using This Tutorial
The tutorials in this repo are provided as Jupyter Notebooks.
These notebooks may be run natively on your system, or you can use docker containers.

### Native Jupyter
After installing you choosen library using anaconda and the guides provided, activate your conda environment, then run the following command:
```
(env)$ jupyter notebook
```
This will start a jupyter server, and should launch a new browser window allowing you to access the server.
If you want to use a different browser than the one Jupyter automatically starts, you can add the `--no-browser' option and copy the URL that Jupyter provides once it is started.
You can now navigate to the tutorial notebooks within Jupyter.

### Docker
You may also use docker containers to run these tutorials.
To do this, you must first install [docker](https://docs.docker.com/get-docker/) on your machine.
Once this is done, you can build the docker containers with the following command (be sure to execute in the root directory of this repo):
```
# docker build -f dockerfiles/tf.dockerfile -t mlexample:tf .
```
Once the container has been built, you can run the container with the following:
```
# docker container run --rm --name mlexample -p 8888:8888 mlexample:tf
```
Note that you may need to use `sudo` depending on how you have docker setup.
In linux, you can add your user to the `docker` group to allow them to use docker without sudo, however be aware that this is effectively giving that user sudo privledges, as there are ways to break out of a docker contrainer with root access.

To use a GPU with these tutorials, you will need to install the [nvidia docker toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html).
Then, when builing the container, use the `tf-gpu.dockerfile` dockerfile.
It is also recommend that you use the tag `tf-gpu` to avoid confusion.
Finally, when running the container, be sure to use the `--gpus all` option to allow the container access to the GPU.

Once the container is started, you can copy the provided URL to your broswer to access Jupyter running on the new container.
Note that you want the choose the URL that has `localhost:8888/`.
If you choose to detach the docker container, you can access the token by running:
```
# docker container exec mlexample jupyter notebook list
```
When you are done, you can stop the container by hitting `ctrl-c`, or if you detached the container, the `docker container stop mlexample` command.

When using docker containers, it is important to remember that any changes you make to the notebooks will not presist once the docker container is removed.
If you want to be able to make changes and save them, you will have to use Jupyter natively, or mount the notbooks to the container.

## Libraries
### Tensorflow
Tensorflow is an open source "high performance numerical computation" library originally developed at Google Brain.
It is highly taylored for Machine Learning and other data science applications.
Tensorflow works by specifying a compute graph that is then optimized.
The graph can then be run to take advantage of different hardware that is available such as multiple CPUs, and Cuda enabled GPUs. Tensorflow 2 has also added eger execution capability allowing users to build and execute compute graphs on the fly. 
Tensorflow is a very powerfull tool and is currently the industry standard.
You can learn more about it at their [website](https://www.tensorflow.org/).
[Here](https://gitlab.com/LikeSmith/MLExample/-/blob/master/TensorFlowExamples/TensorFlowInstallationGuide.md) is the installation guide for tensorflow for the purposes of these tutorials.
The docker tag for our tensorflow containers is `tf`.
It is also the default container and can be accessed through the `latest` tag.

### PyTorch
PyTorch is another open source library designed to allow easy implimentation of Neural Networks and other Deep Learning Tools.  It a python implimentation of the Torch library in Lua, and is primarially developed by Facebook. PyTorch tends to be easier to use on the front end, but does not always scale as well as tensorflow to larger scale learning.  While not quite as pervasive in the research literature as Tensorflow, PyTorch may offer some slightly speedier ad-hoc implimentations that may be worth considering for prototyping and other uses. While we have not yet implimented out turotial using PyTorch, it is on the ToDo list.  Learn more about PyTorch at their [website](https://pytorch.org/).

## Tutorials
### Installation
Before doing anything else, make sure you have all the relevant libraries installed for these tutorials.
The specific installations directions are included for each of the libraries mentioned above, they are all based on using an Anaconda environment.
Anaconda is a system that allows you to create independant python environments, each with their own set of libraries.
This helps avoid dependancy issues and also provides an easy way to install and update various packages.
The packages we will use in addition to the ML libraries discussed above are:

- jupyter
- numpy
- matplotlib
- ipywidgets
- tqdm
- h5py

See the installation instructions for your library of choice for more information.

### Introduction to Machine Learning
This first chapter consists of a series of tutorials that introduce users to the concepts at the core of machine learning.

#### Optimization By Stochastic Gradient Descent
Stochastic Gradient Descent is the algorithm at the heart of most neural network training.
This algorithm is an optimization technique based on batched datasets.
In this tutorial we outline the algorithm, and work an example nonlinear regression problem.

#### Introduction to Neural Networks
This tutorial will build a simple MultiLayered Perceptron by had to classify a set of images of handwritten numerals.
This is a classic Machine Learning problem and an interesting demonstration, and only uses base tensorflow APIs to calculate training gradients.

#### Neural Networks the Easy Way
This tutorial will build the same MNIST classification network, this time using higher level tensorflow APIs and demonstrate the flexibility that tensorflow can offer.

#### Convolutional Neural Networks with CIFAR
This tutorial will discuss a more complex type of neural network, the Convolutional Neural Network.
These networks are common in many image processing applications and are quite powerful.
This tutorial will train on the CIFAR dataset, a collection of labeled RGB images.

#### Regularization
Regularization methods are used to prevent overfitting during training, allowing the model to perform better overall.
We cover several different regularization techniques including L1 and L2 regularization, as well as Dropout and Batch Normalization techniques.

### Unsupervised Learning
This Chapter explores several Unsupervised Learning techniques and demonstrations.

#### Variational AutoEncoders

#### Generative Adversarial Networks

### Reinforcement Learning
This chapter will explore Reinforcement learning, a unique and interesting branch of machine learning that involves exploring a dynamical system and generating data as you go as opposed to learning from a large, static dataset.
The first 3 sections in this chapter are inspired by [Reinforcement Learning: An Introduction by Sutton and Barto](https://books.google.com/books/about/Reinforcement_Learning.html?id=5s-MEAAAQBAJ&source=kp_book_description).
For more details on the topics covered, it is highly recommended as a reference.

#### Introduction to Reiforcment Learning
The fundamentals of Reinforcmeent Learning (RL) are introduced using the simple case of a Multi-Armed Bandit.
Both stationary and non-stationary problems will be explored, and a basic monte-carlo RL algorithm is developed.

#### Markov Decision Processes
The more general Markov Decision Process (MDP) is presented in this tutuorial, and introduce value functions and the bellman equation.
Dynamic Programing and Monte-Carlo solvers will be introduced, and demonstrated on example problems such as GridWorld and Blackjack.

#### SARSA and Q-Learning
Core concepts such as Temporal Difference (TD) learning and boostrapping will be introduced, as well as concepts such as on and off policy learning.
This tutorial will use GridWorld as an example problem that will motivate this discussion.

#### Neural Networks in Reinfocement Learning, DQNs and Actor Critic

#### Proximal Policy Optimization

#### Control Theory and Reinforcement Learning

## Authors
The following contribute to these tutorials, feel free to email them with questions

- Kyle Crandall (crandalk@gmail.com)

## ToDo
- convert chapter 1 tutorials to bokeh plots
- edit tutorial text

θ